import { V2PrayersPage } from './app.po';

describe('v2-prayers App', function() {
  let page: V2PrayersPage;

  beforeEach(() => {
    page = new V2PrayersPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('v2-prayers works!');
  });
});
