export class V2PrayersPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('v2-prayers-app h1')).getText();
  }
}
