import { Component, 
      OnInit,
      Output,
  EventEmitter } 
  from '@angular/core';

import {PrayerData} from '../prayer-data';

@Component({
  moduleId: module.id,
  selector: 'app-newprayer',
  templateUrl: 'newprayer.component.html',
  styleUrls: ['newprayer.component.css']
})
export class NewprayerComponent implements OnInit {

 @Output() createPrayer = new EventEmitter();

  prayerTitle:string;
  englishText:string;
  malayalamText:string;
  

   ngOnInit() {
	 }

  title= 'Add prayer';

  addPrayer() {
    if(this.prayerTitle.length >0 && this.englishText.length >0 &&
    this.malayalamText.length > 0){
      var prayerData = new PrayerData(this.prayerTitle,this.englishText,
    this.malayalamText);
    this.createPrayer.next(prayerData);
    this.prayerTitle = this.malayalamText = this.englishText = '';
    }
    

  }
}
