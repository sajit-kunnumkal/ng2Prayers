import { Component } from '@angular/core';
import {NewprayerComponent} from './newprayer/newprayer.component';
import {PrayerData} from './prayer-data';

@Component({
  moduleId: module.id,
  selector: 'v2-prayers-app',
  templateUrl: 'v2-prayers.component.html',
  styleUrls: ['v2-prayers.component.css'],
  directives: [NewprayerComponent]
})
export class V2PrayersAppComponent {
  title = 'Prayers works!';
  selectedPrayer:PrayerData;
	prayers: Array<PrayerData> = [
        new PrayerData('Angelus','Angel of the Lord..','Karthavinte malaga'),
        new PrayerData('Hail Mary','Hail Mary full of grace..','Namna neranju mariyeme..'),
        new PrayerData('Fatima Prayer','O my Jesus..','O ente Eashoye')
     ];
     	
  

   onAddPrayer(prayer){
    this.prayers.push(prayer);
  }	

   handleChange(prayerId) {
    console.log('Prayer Id',prayerId);
    for(var i=0;i<this.prayers.length;i++) {
      if(this.prayers[i].title === prayerId){
        this.selectedPrayer =  this.prayers[i];
      }
    }
  }
   
}
