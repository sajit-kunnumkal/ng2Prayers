export class PrayerData {
    title: string;
	english: string;
	malayalam: string;
	constructor(title: string,english:string,mal:string) {
        this.title = title;
        this.english = english;
        this.malayalam = mal;
    }
}
