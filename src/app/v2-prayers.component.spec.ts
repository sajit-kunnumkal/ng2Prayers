import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { V2PrayersAppComponent } from '../app/v2-prayers.component';

beforeEachProviders(() => [V2PrayersAppComponent]);

describe('App: V2Prayers', () => {
  it('should create the app',
      inject([V2PrayersAppComponent], (app: V2PrayersAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'v2-prayers works!\'',
      inject([V2PrayersAppComponent], (app: V2PrayersAppComponent) => {
    expect(app.title).toEqual('v2-prayers works!');
  }));
});
