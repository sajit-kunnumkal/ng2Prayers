import { Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {PrayerData} from './prayer-data';

@Injectable()
export class PrayerService {


  prayers : Array<PrayerData>
  public prayerChange: Subject<number> = new Subject<number>();
  constructor() {

     this.prayers = [
        new PrayerData('Angelus','Angel of the Lord..','Karthavinte malaga'),
        new PrayerData('Hail Mary','Hail Mary full of grace..','Namna neranju mariyeme..'),
        new PrayerData('Fatima Prayer','O my Jesus..','O ente Eashoye')
     ];
     
  }


  addPrayer(title:string,english:string,mal:string) {
    var prayer = new PrayerData(title,english,mal)
    this.prayers.push(prayer);
    
     console.log('Length',this.prayers.length);
     this.prayerChange.next(this.prayers.length);
  }

  getPrayers() {
    return this.prayers;
  }

 

}
